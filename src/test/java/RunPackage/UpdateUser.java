package RunPackage;

import io.restassured.RestAssured;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class UpdateUser {
    @Test
    public void UpdateUser(){
        RestAssured.baseURI = "https://reqres.in/api/users/2";
        String userData = "{" +
                "\"name\" : \"mark\", "+
                "\"job\": \"leader\"" +
                "}";
        given().body(userData)
                .when()
                .patch()
                .then().log().all()
                .assertThat()
                .statusCode(200)
                .body("updatedAt", is(notNullValue()));
    }
}
