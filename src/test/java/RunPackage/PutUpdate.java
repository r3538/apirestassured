package RunPackage;

import io.restassured.RestAssured;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class PutUpdate {
    @Test
    public  void put(){
        RestAssured.baseURI = "https://reqres.in/api/users/2";
        String userData = "{" +
                "\"name\" : \"morpheus\", "+
                "\"job\": \"zion resident\"" +
                "}";
        given().body(userData)
                .when()
                .put()
                .then().log().all()
                .assertThat()
                .statusCode(200)
                .body("updatedAt", is(notNullValue()));
    }
}


